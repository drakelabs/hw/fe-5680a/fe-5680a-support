# DrakeLabs Schema Parts Lib

Beginnings of a KiCad parts library. All created with KiCad 5.0+.

## FE-5680A

NOTE: WIP. Especially the FE-5680A, of which there are multiple "FE-5680A" with different pinouts and the same model number. Some have claimed that the P/N differentiates, but I have not seen this fully backed up (included pinout is of the `217400`.
